/**
 * File: App.java
 * @author matth: Matt Wong
 * Editors: Matt Wong, Kyle Fleskes
 * Desc: Contains main and handles incoming message. Also makes a 
 * GUI for easier bot management and monitoring
 */

package Conquer.DiscordRP_Bot;
import javax.security.auth.login.LoginException;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.dv8tion.jda.*;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Channel;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.io.*;

public class App extends ListenerAdapter
{

	private Guild server; 

	//Lists for tracking channels and members in the server
	public static HashMap<String, MessageChannel> channels;
	public static HashMap<String, Member> members;

	public static HashMap<String, PlayerProfile> players;
	public static  HashMap<String, CharacterData> characters;
	public static HashMap<String, Item> items;
	public static HashMap<String, Spell> spells;

	/**
	 * default constructor
	 */
	public App() {

		channels = new HashMap<String, MessageChannel>();
		members = new HashMap<String, Member>();

		players = new HashMap<String, PlayerProfile>();
		characters = new HashMap<String, CharacterData>();

		items = new HashMap<String, Item>();
		spells = new HashMap<String, Spell>();

	}

	/**
	 * Updates channels and members list
	 * Called when channels and members are both empty
	 * @param server
	 */
	private void updateLists(Guild server) {

		channels.clear();
		members.clear();

		//gets and sort members. Removes all bots
		System.out.println("[SETUP]: Reading Members...");
		List<Member> allMembers = server.getMembers();
		for(Member mem : allMembers) {
			if(!mem.getUser().isBot()) {
				System.out.println("[SETUP]: Adding Member: " + mem.getEffectiveName());
				members.put(mem.getUser().getName(), mem);
			}
		}

		//gets and sorts channels. Only takes in text channels
		System.out.println("[SETUP]: Reading Channels...");
		List<Channel> chans = server.getChannels();
		for(Channel chan : chans) {
			if(chan.getType().equals(ChannelType.TEXT)) {
				MessageChannel toAdd = (MessageChannel) chan;
				System.out.println("[SETUP]: Adding Channel: " + toAdd.getName());
				channels.put(toAdd.getName(), toAdd);
			}
		}

		// reads through profiles directory to be read in
		File dir = new File(BotInfo.DBpath + BotInfo.DB_profiles);
		File[] directoryListing = dir.listFiles();
		System.out.println("[SETUP]: Reading Profiles at " + dir.getAbsolutePath() + "...");
		if(!dir.exists())
			System.out.println("[ERROR]: " + dir.getAbsolutePath() + " does not exist");
		if (directoryListing != null) {
			for (File child : directoryListing) {
				if(child.getName().contains("-")) {
					PlayerProfile newPro = new PlayerProfile(child, channels);
					System.out.println("[SETUP]: Adding Player: " + newPro.getName());
					players.put(newPro.getUsername(), newPro);
				}
			}
		}

		// reads through items directory to create dictionary of items
		dir = new File(BotInfo.DBpath + BotInfo.DB_items);
		System.out.println("[SETUP]: Reading Items at " + dir.getAbsolutePath() + "...");
		if(!dir.exists())
			System.out.println("[ERROR]: " + dir.getAbsolutePath() + " does not exist");
		directoryListing = dir.listFiles();
		if (directoryListing != null) {
			for (File child : directoryListing) {
				if(child.getName().contains("-")) {
					Item toAdd = new Item(child);
					System.out.println("[SETUP]: Adding Item: " + toAdd.getName());
					items.put(toAdd.getName(), toAdd);
				}
			}
		}


		// reads through spells directory to create dictionary of spells
		dir = new File(BotInfo.DBpath + BotInfo.DB_spells);
		System.out.println("[SETUP]: Reading Spells at " + dir.getAbsolutePath() + "...");
		if(!dir.exists())
			System.out.println("[ERROR]: " + dir.getAbsolutePath() + " does not exist");
		directoryListing = dir.listFiles();
		if (directoryListing != null) {
			for (File child : directoryListing) {
				if(child.getName().contains("spell_")) {
					Spell toAdd = new Spell(child);
					System.out.println("[SETUP]: Adding Spell: " + toAdd.getName());
					spells.put(toAdd.getName(), toAdd);
				}
			}
		}


		// reads through characters directory to create dictionary of characters
		dir = new File(BotInfo.DBpath + BotInfo.DB_characters);
		System.out.println("[SETUP]: Reading Characters at " + dir.getAbsolutePath() + "...");
		if(!dir.exists())
			System.out.println("[ERROR]: " + dir.getAbsolutePath() + " does not exist");
		directoryListing = dir.listFiles();
		if (directoryListing != null) {
			for (File child : directoryListing) {
				if(child.getName().contains("_cd")) {
					CharacterData toAdd = new CharacterData(child);
					System.out.println("[SETUP]: Adding Character: " + toAdd.getFullName());
					characters.put(toAdd.getName(), toAdd);
				}
			}
		}
	}

	/**
	 * Processes list commands
	 * @param channel
	 * @param args
	 */
	private void listCommands(MessageChannel channel, String[] args) {

		EmbedBuilder eb = MessageType.SYSTEM();
		/**
		 * System print commands that print out information currently stored in system
		 * 
		 * Command:
		 * 	>list profiles
		 * 		-list profiles
		 * 	>list members
		 * 		-list members
		 * 	>list channels
		 * 		-list channels
		 *  >list items
		 *  	-list items
		 *  >list spells
		 *  	-list spells
		 *  >list characters
		 *  	-list all characters by name
		 */
		String toDisp = "";
		int cnt = 1;
		switch(args[1]) {

		case "profiles":
			if(players.isEmpty())
				eb.addField("Empty", "No players in system", false);
			else {
				for(String str: players.keySet()) {
					String toPrint = players.get(str).toString();
					toDisp += cnt + ". " + toPrint + "\n";
					cnt++;
				}
			}
			sendMessage("Players: ", toDisp, true, eb, channel);
			break;

		case "members":
			if(members.isEmpty())
				eb.addField("Empty", "No members in system", false);
			else {
				for(String str: members.keySet()) {
					String toPrint = members.get(str).toString();
					toDisp += cnt + ". " + toPrint + "\n\n";
					cnt++;
				}
			}
			sendMessage("Members: ", toDisp, true, eb, channel);
			break;

		case "channels":
			if(channels.isEmpty())
				eb.addField("Empty", "No channels in system", false);
			else {
				for(String str: channels.keySet()) {
					String toPrint = channels.get(str).toString();
					toDisp += cnt + ". " + toPrint + "\n\n";
					cnt++;
				}
			}
			sendMessage("Channels: ", toDisp, true, eb, channel);
			break;

		case "items":
			if(items.isEmpty())
				eb.addField("Empty", "No items in system", false);
			else {
				for(String str: items.keySet()) {
					String toPrint = items.get(str).toString();
					toDisp += cnt + ". " + toPrint + "\n\n";
					cnt++;
				}
			}
			sendMessage("Items: ", toDisp, true, eb, channel);
			break;

		case "spells":
			if(spells.isEmpty())
				eb.addField("Empty", "No spells in system", false);
			else {
				for(String str: spells.keySet()) {
					String toPrint = spells.get(str).toString();
					toDisp += cnt + ". " + toPrint + "\n\n";
					cnt++;
				}
			}
			sendMessage("Spells: ", toDisp, true, eb, channel);
			break;

		case "characters":
			if(characters.isEmpty())
				eb.addField("Empty", "No characters in system", false);
			else {
				for(String str: characters.keySet()) {
					String toPrint = characters.get(str).toString();
					toDisp += toPrint + "\n";
				}
			}
			sendMessage("Characters: ", toDisp, true, eb, channel);
			break;

		}

	}

	/**
	 * Processes database related commands
	 * @param args
	 */
	private void databaseCommands(MessageChannel channel, String[] args) {

		/**
		 * Database Commands list out file information in the database
		 * 
		 * Commands: 
		 * 	>db path 
		 * 		- prints the path of the database
		 * 	>db (profiles|players|characters|spells|items)
		 * 		- prints all the files found in the respectful path
		 */

		EmbedBuilder eb = MessageType.DB();
		File dir;
		File[] directoryListing;
		String toPrint;

		if(args.length <= 1) {
			sendMessage("Path of Database", BotInfo.DBpath, false, eb, channel);
			return;
		}

		switch(args[1]) {

		//simple command that prints out path of the database
		case "path":
			sendMessage("Path of Database", BotInfo.DBpath, false, eb, channel);
			break;

		case "profiles":
			toPrint = "";
			dir = new File(BotInfo.DBpath + BotInfo.DB_profiles);
			directoryListing = dir.listFiles();
			if (directoryListing != null) {
				int i = 1;
				for (File child : directoryListing) {
					if(child.getName().contains(".txt")) {
						toPrint += i + ". " + child.getName() + "\n";
						i++;
					}
				}
			}

			sendMessage("All Profile Files at " + BotInfo.DBpath + BotInfo.DB_profiles, toPrint, false, eb, channel);
			break;

		case "items":
			toPrint = "";
			dir = new File(BotInfo.DBpath + BotInfo.DB_items);
			directoryListing = dir.listFiles();
			if (directoryListing != null) {
				int i = 1;
				for (File child : directoryListing) {
					if(child.getName().contains(".txt")) {
						toPrint += i + ". " + child.getName() + "\n";
						i++;
					}
				}
			}
			sendMessage("All Item Files at " + BotInfo.DBpath + BotInfo.DB_items, toPrint, false, eb, channel);
			break;

		case "spells":
			toPrint = "";
			dir = new File(BotInfo.DBpath + BotInfo.DB_spells);
			directoryListing = dir.listFiles();
			if (directoryListing != null) {
				int i = 1;
				for (File child : directoryListing) {
					if(child.getName().contains(".txt")) {
						toPrint += i + ". " + child.getName() + "\n";
						i++;
					}
				}
			}

			sendMessage("All Spell Files at " + BotInfo.DBpath + BotInfo.DB_spells, toPrint, false, eb, channel);
			break;

		case "characters":
			toPrint = "";
			dir = new File(BotInfo.DBpath + BotInfo.DB_characters);
			directoryListing = dir.listFiles();
			if (directoryListing != null) {
				int i = 1;
				for (File child : directoryListing) {
					if(child.getName().contains(".txt")) {
						toPrint += i + ". " + child.getName() + "\n";
						i++;
					}
				}
			}

			sendMessage("All Character Files at " + BotInfo.DBpath + BotInfo.DB_characters, toPrint, false, eb, channel);
			break;
		}
	}

	/**
	 * Processes 'create' commands
	 * @param channel
	 * @param msg
	 * @param args
	 */
	private void createCommands(MessageChannel channel, String msg, String[] args) {

		ArrayList<String> quoteExtract = extractQuotes(msg);
		EmbedBuilder eb = MessageType.SYSTEM();

		switch(args[1]) {

		/*
		 * Adds a new player profile and saves it to database
		 * 
		 * Command:
		 * >create player <name> <email> <username>
		 * or
		 * >create player "<name>" "<email>" "<username>"
		 */
		case "player":

			// check error case
			if((args.length > 5 && quoteExtract.size() != 3) || args.length < 5) {
				eb = MessageType.ERROR();

				sendMessage("Invalid Arguments", "Invalid arguments for 'create player' command", false, eb, channel);
				return;
			}
			else {
				String arg1 = "";
				String arg2 = "";
				String arg3 = "";

				if(quoteExtract.size() > 0) {
					arg1 = quoteExtract.get(0);
					arg2 = quoteExtract.get(1);
					arg3 = quoteExtract.get(2);
				}
				else {
					arg1 = args[2];
					arg2 = args[3];
					arg3 = args[4];
				}

				//search for member of server to match arg3
				for(String memStr: members.keySet()) {
					Member mem = members.get(memStr);
					//if matches a member, create a new player profile
					if(mem.getEffectiveName().equals(arg3) || mem.getUser().getName().equals(arg3)) {
						PlayerProfile toAdd = new PlayerProfile(arg1, arg2, mem.getUser());
						players.put(mem.getUser().getName(), toAdd);

						sendMessage("Success! New Player Created:", toAdd.toString(), false, eb, channel);
						return;
					}		
				}

				eb = MessageType.ERROR();
				sendMessage("Member Not Found", "Member specified not found, username not valid", false, eb, channel);
			}
			break;

			/**
			 * Adds a new character to the database
			 * Command:
			 * >create character "<full name>" "<nickname>" "<gender>" "<race>" "<age>"
			 */
		case "character":
			if(quoteExtract.size() != 5) {
				eb = MessageType.ERROR();
				sendMessage("Invalid Arguments", "Invalid arguments for 'create character' command", false, eb, channel);
				return;
			}
			else {
				String arg1 = quoteExtract.get(0);
				String arg2 = quoteExtract.get(1);
				String arg3 = quoteExtract.get(2);
				String arg4 = quoteExtract.get(3);
				String arg5 = quoteExtract.get(4);

				CharacterData toAdd = new CharacterData(arg1, arg2, arg3, arg4, arg5);
				characters.put(toAdd.getName(), toAdd);

				sendMessage("Success! New Character '" + toAdd.getName() + "' Created:", toAdd.toString(), false, eb, channel);
			}
			break;

			/**
			 * Adds a new spell to the database
			 * Command
			 * >create spell <name> <description> <cost>
			 */
		case "spell":
			if(quoteExtract.size() != 3) {
				eb = MessageType.ERROR();
				sendMessage("Invalid Arguments", "Invalid arguments for 'create spell' command", false, eb, channel);
				return;
			}
			else {
				String arg1 = quoteExtract.get(0);
				String arg2 = quoteExtract.get(1);
				int arg3;
				try {
					arg3 = Integer.parseInt(quoteExtract.get(2));
				} catch(Exception e) {
					eb = MessageType.ERROR();
					sendMessage("Invalid Arguments", "Invalid arguments for 'create spell' command", false, eb, channel);
					return;
				}

				Spell toAdd = new Spell(arg1, arg2, arg3);
				spells.put(toAdd.getName(), toAdd);

				sendMessage("Success! New Spell Created:", toAdd.toString(), false, eb, channel);

			}
			break;

			/*
			 * Adds a new generic item and saves it to database
			 * Command:
			 * >create (item|equipment|currency|consumable|important|misc) "<name>" "<description>" "<weight>"
			 */
		case "item":
			createItem(quoteExtract, eb, channel, "misc");
			break;

		case "equipment":
			createItem(quoteExtract, eb, channel, "equipment");
			break;

		case "currency":
			createItem(quoteExtract, eb, channel, "currency");
			break;

		case "consumable":
			createItem(quoteExtract, eb, channel, "consumable");
			break;

		case "important":
			createItem(quoteExtract, eb, channel, "important");
			break;

		case "misc":
			createItem(quoteExtract, eb, channel, "misc");
			break;
		}
	}

	/**
	 * Helper to the createCommands function to help create items
	 * @param quoteExtract
	 * @param eb
	 * @param channel
	 * @param type
	 */
	private void createItem(ArrayList<String> quoteExtract, EmbedBuilder eb, MessageChannel channel, String type) {
		// check error case
		if(quoteExtract.size() != 3) {
			eb = MessageType.ERROR();
			sendMessage("Invalid Arguments", "Invalid arguments for 'create " + type +"' command", false, eb, channel);
			return;
		}
		else {

			String arg1 = quoteExtract.get(0);
			String arg2 = quoteExtract.get(1);
			// checks for double
			double arg3;
			try {
				arg3 = Double.parseDouble(quoteExtract.get(2));
			} catch(Exception e) {
				eb = MessageType.ERROR();
				sendMessage("Invalid Arguments", "Invalid arguments for 'create " + type + "' command", false, eb, channel);
				return;
			}
			Item toAdd = new Item(arg1, arg2, arg3, type);
			items.put(arg1, toAdd);
			sendMessage("Success! New " + type + " Created:", toAdd.toString(), false, eb, channel);
		}
	}

	/**
	 * Edits parameters of entity
	 * @param channel
	 * @param msg
	 * @param args
	 */
	private void editCommands(MessageChannel channel, String msg, String[] args) {

	}

	/**
	 * Assign commands that assign entities or add data to existing objects
	 * @param channel
	 * @param msg
	 * @param args
	 */
	private void assignCommands(MessageChannel channel, String msg, String[] args) {

		/**
		 * Commands:
		 * 
		 * assign <player> (channel|character) <character or channel name>
		 * 	- Assigns player the channel or character
		 */
		ArrayList<String> quoteExtract = extractQuotes(msg);
		EmbedBuilder eb = MessageType.GENERAL();

		if(quoteExtract.size() != 2 || quoteExtract.size() == 0 || args.length != 4) {
			eb = MessageType.ERROR();
			sendMessage("Invalid Arguments", "Invalid arguments for 'assign <player>' command", false, eb, channel);
			return;
		}

		String name = quoteExtract.get(0);
		if(!players.containsKey(name)) {
			eb = MessageType.ERROR();
			sendMessage("Invalid Player", "Member " + name + " needs to be a Player before anything can be assigned", false, eb, channel);
			return;
		}

		PlayerProfile p = players.get(name);
		String type = args[2];
		String n = quoteExtract.get(1);
		String toPrint = "";

		//if entered character exists in DB, then assign to player, if not, assign new default character
		if(type.equals("character")) {
			if(characters.containsKey(n)) {
				p.setCharacter(characters.get(n));
				toPrint = name + " has been assigned the character, " + n;
			} else {
				eb = MessageType.ERROR();
				sendMessage("Invalid Character", "'" + n + "' does not exist in the database as a character", false, eb, channel);
				return;
			}
			players.put(name, p);
			sendMessage("Success", toPrint, false, eb, channel);
		}

		//if entered channel exists, then assign player, if not, throw error
		else if(type.equals("channel")) {
			if(channels.containsKey(n)) {
				p.setChannel(channels.get(n));
				sendMessage("Success", name + " has been assigned channel, " + n, false, eb, channel);
			}
			else {
				eb = MessageType.ERROR();
				sendMessage("Channel Does not Exist", "'" + n +"' is not a valid/existing message channel", false, eb, channel);
			}
		}

		else {
			sendMessage("Invalid Argument", "Invalid type of assignment for " + p, false, eb, channel);

		}
	}

	/**
	 * Commands to show a list of details of specified entities that matches string.
	 * This command checks everything by name but has to be exact match
	 * @param channel
	 * @param msg
	 * @param args
	 */
	private void showCommands(MessageChannel channel, String msg, String[] args) {
		/**
		 * Commands:
		 * 	>show "name"
		 * 		-Finds all results with name
		 */
		ArrayList<String> quoteExtract = extractQuotes(msg);
		EmbedBuilder eb = MessageType.GENERAL();

		ArrayList<Object> list = new ArrayList<Object>();

		if(quoteExtract.size() == 1) {
			String toFind = quoteExtract.get(0);

			if(players.containsKey(toFind))
				list.add(players.get(toFind));
			if(characters.containsKey(toFind))
				list.add(characters.get(toFind));
			if(items.containsKey(toFind)) 
				list.add(items.get(toFind));
			if(spells.containsKey(toFind))
				list.add(spells.get(toFind));

			String toPrint = "";
			for(int i=0; i<list.size(); i++) 
				toPrint += (i+1) + ". \n" + list.get(i).toString() + "\n";

			sendMessage("Results of " + toFind, toPrint, true, eb, channel);

		}	
		else {
			sendMessage("Invalid Argument", "Invalid argument for 'show' command", false, eb, channel);
		}
	}

	/**
	 * Search command is much like the Show command but search checks everything
	 * that has the passed in string. Case does not matter
	 * @param channel
	 * @param msg
	 * @param args
	 */
	private void searchCommands(MessageChannel channel, String msg, String[] args) {
		/**
		 * Commands:
		 * 	>search "arg1" "arg2" ... "argn"
		 * 		-Finds all results with name
		 */
		ArrayList<String> quoteExtract = extractQuotes(msg);
		EmbedBuilder eb = MessageType.GENERAL();

		ArrayList<Entity> list = new ArrayList<Entity>();

		if(quoteExtract.size() >= 1) {
			String toFind = quoteExtract.get(0).toLowerCase();

			for(String i : players.keySet()) {
				PlayerProfile cur = players.get(i);		
				if(cur.allStrings().contains(toFind))
					list.add(cur);
			}

			for(String i : characters.keySet()) {
				CharacterData cur = characters.get(i);		
				if(cur.allStrings().contains(toFind))
					list.add(cur);
			}

			for(String i : items.keySet()) {
				Item cur = items.get(i);		
				if(cur.allStrings().contains(toFind))
					list.add(cur);
			}

			for(String i : spells.keySet()) {
				Spell cur = spells.get(i);		
				if(cur.allStrings().contains(toFind))
					list.add(cur);
			}

			//loops through the rest of the arguments and filter
			for(int i=1; i<quoteExtract.size(); i++) {
				ArrayList<Entity> filtered = new ArrayList<Entity>();
				toFind = quoteExtract.get(i).toLowerCase();

				for(int j=0; j<list.size(); j++) {
					if(list.get(j).allStrings().contains(toFind))
						filtered.add(list.get(j));
				}

				list = filtered;
			}

			String toPrint = "";
			for(int i=0; i<list.size(); i++) 
				toPrint += (i+1) + ". \n" + list.get(i).toString() + "\n";

			sendMessage("Results of " + quoteExtract.toString(), toPrint, false, eb, channel);

		}	
		else {
			sendMessage("Invalid Argument", "Invalid argument for 'search' command", false, eb, channel);
		}
	}

	/**
	 * Commands for management of character inventories. Admin commands only
	 * @param channel
	 * @param msg
	 * @param args
	 */
	private void inventoryAdminCommands(MessageChannel channel, String msg, String[] args) {
		ArrayList<String> quoteExtract = extractQuotes(msg);
		EmbedBuilder eb = MessageType.INVENTORY();

		if(args.length < 2 || quoteExtract.isEmpty()) {
			sendMessage("Invalid Command for Admin", "Invalid Command for Admin", false, MessageType.ERROR(), channel);
			return;
		}

		String toFind = quoteExtract.get(0);

		//checks matching nicknames too 
		CharacterData toEdit = null;
		for(String c : characters.keySet()) {
			if(characters.get(c).getNickname().equals(toFind)) {
				toEdit = characters.get(c);
				break;
			}
		}

		if(toEdit == null) {
			//finds passed in character or player
			if(characters.containsKey(toFind)) 
				toEdit = characters.get(toFind);
			else if(players.containsKey(toFind))
				toEdit = players.get(toFind).getHero();
			else {
				eb = MessageType.ERROR();
				sendMessage("Cannot Find " + toFind, "Cannot find " + toFind + " as a character nor player", false, eb, channel);
				return;
			}
		}

		//case that a found player does not have a character
		if(toEdit == null) {
			eb = MessageType.ERROR();
			sendMessage("Player does not have an assigned character", toFind + " does not have an assigned character", false, eb, channel);
			return;
		}

		toEdit.getInventory().refresh();
		// one arguments to show an inventory
		/**
		 * Commands:
		 * 	>inventory "<character/player name>"
		 * 		-Shows the entire inventory of specified character/player. Characters have precedence
		 */
		if(args.length == 2 && quoteExtract.size() == 1) {
			if(toEdit != null) {
				sendMessage(toEdit.getName() + "'s Inventory", toEdit.getInventory().toString(), true, eb, channel);
			} 
		}

		//two arguments
		/**
		 * Commands:
		 * >inventory "<character|player name>" clear
		 * 	-clears player or character's inventory
		 * >inventory "<character|player name>" (money|currency|equipment|consumable|important|misc)
		 * 	-prints the entire category in specified character's inventory
		 */
		else if(args.length == 3 && quoteExtract.size() == 1) {
			String command = args[2];
			if(command.equals("clear")) {
				toEdit.clearInventory();
				characters.put(toEdit.getName(), toEdit);
				sendMessage("Inventory Cleared", toEdit.getName() + "'s inventory has been cleared", false, eb, channel);
			}
			else {
				String cat = toEdit.getCategory(command);
				if(cat != null) {
					sendMessage(toFind + "'s " + command + "s", cat, true, eb, channel);
				}
				else {
					eb = MessageType.ERROR();
					sendMessage("Invalid Command", "Invalid command for 'inventory' command", false, eb, channel);
				}
			}
		}	

		//three or four arguments with two quotes
		/**
		 * Commands:
		 * >inventory "<character|player name>" add "<item>"
		 * 	-adds in a specified item
		 * >inventory "<character|player name>" add "<item>" <integer>
		 * 	-adds in specified number of items
		 * >inventory "<character|player name>" remove "<item>"
		 * 	-remove a specified item
		 * >inventory "<character|player name>" remove "<item>" <integer>
		 * 	-remove a specified amount of item
		 * >inventory "<character|player name>" get "<item>"
		 * 	-gets specified items 
		 */
		else if((args.length == 4 || args.length == 5) && quoteExtract.size() == 2 && !args[2].equals("search")) {
			String arg = quoteExtract.get(1);
			String command = args[2];
			//checks if item exists in database
			Item item = null;
			if(items.containsKey(arg))
				item = items.get(arg);

			int num = 1;

			switch(command) {

			case "add":
				//case that item was not found in DB, create a new item, default category in misc and weight 0
				if(item == null) {
					sendMessage("New Item '" + arg + " created", "Item '" + arg + "' does not exist in the database. Added as a new item with default settings", false, MessageType.WARNING(), channel);
					item = new Item(arg, "", 0.0, "misc");
					items.put(arg, item);
				}
				//checks if there are 4 arguments, if there are, check if the 4th argument is a number and add
				//specified amount into inventory. Else, add 1
				if(args.length == 5) {
					try {
						num = Integer.parseInt(args[4]);
					} catch(Exception e) {
						eb = MessageType.ERROR();
						sendMessage("Invalid Input", "Invalid Input for 'inventory' command. Must be a valid number", false, eb, channel);
						return;
					}
					if(num < 0) {
						eb = MessageType.ERROR();
						sendMessage("Invalid Input", "Please enter a positive integer", false, eb, channel);
						return;
					}
					toEdit.addToInventory(item, num);
				}
				else
					toEdit.addToInventory(item, 1);

				characters.put(toEdit.getName(), toEdit);
				sendMessage(toEdit.getName() + "'s Inventory", "Added " + num + " " + item.getName() + "s", true, eb, channel);
				break;

			case "remove":
				//case that item was not found in DB, if not, throw error
				if(item == null) {
					eb = MessageType.ERROR();
					sendMessage("Invalid Item", "Item " + arg + " does not exist in database", false, eb, channel);
					return;
				}
				//checks if there are 4 arguments, if there are, check if the 4th argument is a number and remove
				//specified amount from inventory. Else, remove 1
				if(args.length == 5) {
					try {
						num = Integer.parseInt(args[4]);
					} catch(Exception e) {
						eb = MessageType.ERROR();
						sendMessage("Invalid Input", "Invalid Input for 'inventory' command. Must be a valid number", false, eb, channel);
					}

					if(num < 0) {
						eb = MessageType.ERROR();
						sendMessage("Invalid Input", "Please enter a positive integer", false, eb, channel);
						return;
					}

					toEdit.removeFromInventory(item, num);
				}
				else
					toEdit.removeFromInventory(item, 1);
				characters.put(toEdit.getName(), toEdit);

				sendMessage(toEdit.getName() + "'s Inventory", "Removed " + num + " " + item.getName() + "s", true, eb, channel);
				break;

			case "get":
				//case that item was not found in DB, if not throw error
				if(item == null) {
					eb = MessageType.ERROR();
					sendMessage("Invalid Item", "Item " + arg + " does not exist in database", false, eb, channel);
					return;
				}

				num = toEdit.getItemInInventory(item);
				if(num == 0)
					sendMessage(item.getName() + " - " + num, "None Found in Inventory", true, eb, channel); 
				else
					sendMessage(item.getName() + " - " + num, item.toString(), true, eb, channel);
				break;		
			}
		}

		//for three arguments or more with 2 or more quotes
		/**
		 * Commands:
		 * 	>inventory "<players or character>" search "arg1" "arg2".... "argn"
		 */
		else if(args.length >= 4 && quoteExtract.size() >= 2) {
			String arg = quoteExtract.get(1);
			String command = args[2];

			if(command.equals("search")) {
				HashMap<Item, Integer> results = toEdit.search(arg.toLowerCase());

				//filters the remaining arguments
				HashMap<Item, Integer> filtered = new HashMap<Item, Integer>();
				for(int i=1; i<quoteExtract.size(); i++) {
					toFind = quoteExtract.get(i).toLowerCase();
					for(Item j : results.keySet()) {
						if(j.allStrings().contains(toFind))
							filtered.put(j, results.get(j));
					}

					results = filtered;
				}

				String toPrint = "";
				int cnt = 1;
				for(Item i : results.keySet()) {
					toPrint += cnt + ". " + i.name + " - " + results.get(i) +"\n"
							+ i.toString() + "\n";
					cnt++;
				}

				quoteExtract.remove(0);
				sendMessage("Results for " + quoteExtract.toString(), toPrint, true, eb, channel);
			}
			else {
				eb = MessageType.ERROR();
				sendMessage("Invalid Arguments", "Invalid Arguments for 'inventory' command", false, eb, channel);
			}
		} 
		else {
			eb = MessageType.ERROR();
			sendMessage("Invalid Arguments", "Invalid Arguments for 'inventory' command", false, eb, channel);
		}
	}

	/**
	 * Unrestricted inventory commands for players to use. Only can use to access own inventory
	 * @param channel
	 * @param msg
	 * @param args
	 * @param user
	 */
	private void inventoryCommands(MessageChannel channel, String msg, String[] args, User user) {
		/**
		 * Commands:
		 * >inventory
		 * 	-Displays entire inventory
		 * >inventory (money|currency|equipment|consumable|important|misc)
		 * 	-Displays entire category of specified category
		 * >inventory get "(item name)"
		 * 	-Gets the specified item from inventory
		 * >inventory search "arg1" "arg2" "arg3" .... "argn"
		 * 	-Searches through entire inventory with arguments
		 */
		ArrayList<String> quoteExtract = extractQuotes(msg);
		EmbedBuilder eb = MessageType.INVENTORY();

		PlayerProfile profile = players.get(user.getName());

		//sets default channel to whatever the player's channel is
		if(profile != null && profile.getChannel() != null)
			channel = profile.getChannel();

		//checks player has a profile and a character
		if(profile == null || profile.getHero() == null) {
			sendMessage("No Profile or Character Found", "You must have a profile and a character before you can access your inventory", false, MessageType.ERROR(), channel);
			return;
		}

		CharacterData character = profile.getHero();
		character.getInventory().refresh();

		//if only one argument, meaning just ">inventory", display entire inventory
		if(args.length == 1) {
			sendMessage(character.getName() + "'s Inventory", character.getInventory().toString(), true, eb, channel);
		} 

		//if two arguments 
		else if(args.length == 2) {
			String cat = args[1];
			cat = character.getCategory(cat);
			if(cat != null) {
				sendMessage(character.getName() + "'s " + args[1] + "s", cat, true, eb, channel);
			}
			else {
				eb = MessageType.ERROR();
				sendMessage("Invalid Command", "Invalid command for 'inventory' command", false, eb, channel);
			}
		}

		//if three arguments 
		else if(args.length == 3 && quoteExtract.size() == 1 && !args[1].equals("search")) {
			String comm = args[1];
			if(comm.equals("get")) {
				String itemName = quoteExtract.get(0);

				if(!items.containsKey(itemName)) {
					eb = MessageType.ERROR();
					sendMessage("Invalid Item", "Item '" + itemName + "' does not exist in the database", false, eb, channel);
					return;
				}

				int amount = character.getItemInInventory(items.get(itemName));
				if(amount == 0) {
					sendMessage("No " + itemName + " found", "There are no " + itemName + "s in your inventory", true, eb, channel);
				}
				else {
					sendMessage(itemName + " - " + amount, items.get(itemName).toString(), true, eb, channel);
				}
			}
		}

		//for search command
		else if(args.length >= 3 && quoteExtract.size() >= 1 && args[1].equals("search")) {
			String comm = args[1];

			if(comm.equals("search")) {

				//search through inventory and filter with arguments
				HashMap<Item, Integer> results = character.search(quoteExtract.get(0).toLowerCase());
				HashMap<Item, Integer> filter = new HashMap<Item, Integer>();
				for(int i=2; i<quoteExtract.size(); i++) {
					for(Item j : results.keySet()) {
						if(j.allStrings().contains(quoteExtract.get(i).toLowerCase())) {
							filter.put(j, results.get(j));
						}
					}

					results = filter;
				}

				int cnt = 1;
				String toPrint = "";
				for(Item i : results.keySet()) {
					toPrint += cnt + ". " + i.getName() + " - " + results.get(i) + "\n"
							+ i.toString() + "\n";
					cnt++;
				}

				quoteExtract.remove(0);
				sendMessage("Results for " + quoteExtract.toString(), toPrint, true, eb, channel);
			}
		}
		else {
			eb = MessageType.ERROR();
			sendMessage("Invalid Arguments", "Invalid Arguments for 'inventory' command", false, eb, channel);
		}
	}

	/**
	 * Spellbook commands for the admin
	 * @param channel
	 * @param msg
	 * @param args
	 */
	private void spellbookAdminCommands(MessageChannel channel, String msg, String[] args) {

		/**
		 * Commands:
		 * 	>spellbook "<character/player>"
		 * 		-shows spellbook and list spells
		 * 	>spellbook "<character/player>" add "<spellname>"
		 * 		-adds spell to spellbook, spell has to be in the database
		 * 	>spellbook "<character/player>" remove "<spellname>"
		 * 		-removes spell from spellbook
		 *  >spellbook "<character/player>" cast "<spellname>"
		 *  	-force cast a spell
		 * 	>spellbook "<character/player>" search "<args1>" "<args2"...."<argsn>"
		 */

		ArrayList<String> quoteExtract = extractQuotes(msg);
		EmbedBuilder eb = MessageType.SPELLBOOK();
		if((args.length < 2) || (quoteExtract.size() == 0)) {
			sendMessage("Invalid Command for Admin", "Invalid Command for Admin", false, MessageType.ERROR(), channel);
			return;
		}

		CharacterData character = null;

		//checks nicknames as well
		for(String c : characters.keySet()) {
			if(characters.get(c).getNickname().equals(quoteExtract.get(0))) {
				character = characters.get(c);
			}
		}

		if(character == null) {
			if(!characters.containsKey(quoteExtract.get(0))) {
				if(!players.containsKey(quoteExtract.get(0)))  {
					sendMessage("Invalid Player or Character", quoteExtract.get(0) + " not found as a player or character", false, MessageType.ERROR(), channel);
					return;
				}
				else 
					character = players.get(quoteExtract.get(0)).getHero();
			}
			else 
				character = characters.get(quoteExtract.get(0));
		}

		character.getSpellbook().refresh();
		//for commands with two arguments 
		if(args.length == 2 && quoteExtract.size() == 1) {
			sendMessage(character.getName() + "'s Spellbook", character.getSpellbook().toString(), false, eb, channel);
		}
		//for commands with four arguments
		else if(args.length == 4 && quoteExtract.size() == 2 && !args[2].equals("search")) {
			String comm = args[2];
			String arg = quoteExtract.get(1);
			//add command
			if(comm.equals("add")) {
				if(spells.containsKey(quoteExtract.get(1))) {
					character.addSpell(spells.get(quoteExtract.get(1)));
					sendMessage("Spell Added", "Added " + quoteExtract.get(1) + " to " + character.getName() + "'s Spellbook", false, eb, channel);
				}
				else {
					sendMessage("Invalid Spell", quoteExtract.get(1) + " was not found in the database", false, MessageType.ERROR(), channel);
				}
			}

			//remove command
			else if(comm.equals("remove")) {
				if(spells.containsKey(quoteExtract.get(1))) {
					character.removeSpell(spells.get(quoteExtract.get(1)).getName());
					sendMessage("Spell Removed", "Removed " + quoteExtract.get(1) + " from " + character.getName() + "'s Spellbook", false, eb, channel);
				}
				else {
					sendMessage("Invalid Spell", quoteExtract.get(1) + " was not found in the database", false, MessageType.ERROR(), channel);
				}
			}

			else if(comm.equals("cast")) {
				if(spells.containsKey(arg) && character.getSpellbook().getSpellbook().containsKey(arg)) {
					boolean success = character.cast(arg);
					if(success)
						sendMessage(arg + " Casted", character.getName() + " casted " + arg + "\n MP: " + character.getMP(), false, eb, channel);
					else
						sendMessage("Failed Cast", character.getName() + " does not have enough MP to cast " + arg + " (Cost: " + spells.get(arg).getCost() + ")\n" + "MP: " + character.getMP(), false, eb, channel);
				}
				else 
					sendMessage("Invalid Spell", arg + " is either not a valid spell or not in " + character.getName() + "'s spellbook", false, MessageType.ERROR(), channel);
			}
			else {
				sendMessage("Invalid Command", "Invalid Command for Spellbook", false, MessageType.ERROR(), channel);
			}
		}

		//commands with four or more arguments
		else if(args.length >= 4 && quoteExtract.size() >= 2) {
			String comm = args[2];

			if(comm.equals("search")) {

				ArrayList<Spell> results = new ArrayList<Spell>();
				HashMap<String, Spell> sp = character.getSpellbook().getSpellbook();
				for(String curSp : sp.keySet()) {
					if(sp.get(curSp).allStrings().contains(quoteExtract.get(1).toLowerCase()))
						results.add(sp.get(curSp));
				}

				for(int i=2; i<quoteExtract.size(); i++) {
					ArrayList<Spell> newResults = new ArrayList<Spell>();
					for(Spell curS : results) {
						if(curS.allStrings().contains(quoteExtract.get(i).toLowerCase()))
							newResults.add(curS);
					}

					results = newResults;
				}

				String strResults = "";
				int cnt = 1;
				for(Spell cur : results) {
					strResults += cnt + ". " + cur.toString() + "\n";
					cnt++;
				}

				quoteExtract.remove(0);
				sendMessage("Results of " + quoteExtract.toString() + " in " + character.getName() + "'s Spellbook", strResults, true, eb, channel);
			}
			else {
				sendMessage("Invalid Command", "Invalid Command for Spellbook", false, MessageType.ERROR(), channel);
			}
		}
	}

	/**
	 * Spellbook commands for the player
	 * @param channel
	 * @param msg
	 * @param args
	 * @param user
	 */
	private void spellbookCommands(MessageChannel channel, String msg, String[] args, User user) {
		/**
		 * Commands:
		 * 	>spellbook
		 * 		Displays entire spellbook
		 * 	>spellbook get "(spell name)"
		 * 		Displays info about specified spell
		 *	>spellbook search "(arg1)" "(arg2)"... "(argn)"
		 */
		ArrayList<String> quoteExtract = extractQuotes(msg);
		EmbedBuilder eb = MessageType.SPELLBOOK();

		PlayerProfile profile = players.get(user.getName());

		//sets default channel to whatever the player's channel is
		if(profile != null && profile.getChannel() != null)
			channel = profile.getChannel();

		//checks player has a profile and a character
		if(profile == null || profile.getHero() == null) {
			sendMessage("No Profile or Character Found", "You must have a profile and a character before you can access your spellbook", false, MessageType.ERROR(), channel);
			return;
		}

		CharacterData character = profile.getHero();
		character.getSpellbook().refresh();
		character.refresh();

		//if only one argument, meaning just ">spellbook", display entire spellbook
		if(args.length == 1 && quoteExtract.size() == 0) {
			sendMessage(character.getName() + "'s Spellbook", character.getSpellbook().toString(), true, eb, channel);
		} 

		//for three arguments
		else if(args.length == 3 && quoteExtract.size() == 1 && !args[1].equals("search")) {
			String comm = args[1];
			if(comm.equals("get")) {
				String toFind = quoteExtract.get(0);

				if(character.findSpell(toFind) != null) {
					sendMessage("Spell: " + toFind, character.findSpell(toFind).toString(), true, eb, channel);
				}
				else {
					sendMessage("Cannot find " + toFind, "The spell, '" + toFind + "' is not in your spellbook", true, eb, channel);
				}
			}
			else
				sendMessage("Invalid Command", "Invalid Command for Spellbook", false, MessageType.ERROR(), channel);
		}

		//for three or more arguments
		else if(args.length >= 3 && quoteExtract.size() >= 1) {
			String comm = args[1];

			if(comm.equals("search") ) {
				HashMap<String, Spell> sb = character.getSpellbook().getSpellbook();
				ArrayList<Spell> results = new ArrayList<Spell>();

				for(String s : sb.keySet()) {
					if(sb.get(s).allStrings().contains(quoteExtract.get(0).toLowerCase())) {
						results.add(sb.get(s));
					}
				}

				for(int i = 2; i < quoteExtract.size(); i++) {
					ArrayList<Spell> nResults = new ArrayList<Spell>();
					for(Spell s : results) {
						if(s.allStrings().contains(quoteExtract.get(i).toLowerCase()))
							nResults.add(s);
					}	
					results = nResults;
				}

				int cnt = 1;
				String toDisp = "";
				for(Spell s : results) {
					toDisp += cnt + ". " + s.toString() + "\n";
					cnt++;
				}

				sendMessage("Results of " + quoteExtract.toString() + " in " + character.getName() + "'s Spellbook", toDisp, true, eb, channel);
			}
			else {
				sendMessage("Invalid Command", "Invalid Command for Spellbook", false, MessageType.ERROR(), channel);
			}
		}
	}


	/**
	 * MP commands for the admin
	 * @param channel
	 * @param msg
	 * @param args
	 */
	private void mpAdminCommands(MessageChannel channel, String msg, String[] args) {
		/**
		 * Commands:
		 *  >mp <player/character>
		 *  	-shows character's current mp
		 *  >mp <player/character> max
		 *  	-shows character's max mp
		 *  >mp <player/character> restore
		 *  	-restores character's mp to max
		 *  >mp <player/character> set <integer>
		 *  	-sets character's mp to something
		 *  >mp <player/character> setmax <integer>
		 *  >mp <player/character> add <integer>
		 *  	-adds mp to character
		 *  >mp <player/character> subtract <integer>
		 *  	-subtract mp to character
		 */

		ArrayList<String> quoteExtract = extractQuotes(msg);
		EmbedBuilder eb = MessageType.MP();
		if((args.length < 2) || (quoteExtract.size() == 0)) {
			sendMessage("Invalid Command for Admin", "Invalid Command for Admin", false, MessageType.ERROR(), channel);
			return;
		}

		CharacterData character = null;

		//checks nicknames as well
		for(String c : characters.keySet()) {
			if(characters.get(c).getNickname().equals(quoteExtract.get(0))) {
				character = characters.get(c);
				break;
			}
		}

		if(character == null) {
			if(!characters.containsKey(quoteExtract.get(0))) {
				if(!players.containsKey(quoteExtract.get(0)))  {
					sendMessage("Invalid Player or Character", quoteExtract.get(0) + " not found as a player or character", false, MessageType.ERROR(), channel);
					return;
				}
				else 
					character = players.get(quoteExtract.get(0)).getHero();
			}
			else 
				character = characters.get(quoteExtract.get(0));
		}

		character.refresh();
		//for commands with two arguments 
		if(args.length == 2 && quoteExtract.size() == 1) {
			sendMessage(character.getName() + "'s Current MP", character.getMP() + "/" + character.getMaxMP(), false, eb, channel);
		}

		//for commands with three arguments
		else if(args.length == 3 && quoteExtract.size() == 1) {
			String comm = args[2];

			if(comm.equals("max")) {
				sendMessage(character.getName() + "'s Max MP", character.getMaxMP() + "", false, eb, channel);
			}
			else if(comm.equals("restore")) {
				character.restoreMP();
				sendMessage("MP Restored", character.getName() + "'s MP Restored to " + character.getMP() + "", false, eb, channel);
			}
			else {
				sendMessage("Invalid Command", "Invalid Command for MP", false, MessageType.ERROR(), channel);
			}
		}

		//for commands with four arguments
		else if(args.length == 4 && quoteExtract.size() == 1) {
			String comm = args[2];
			int toMod = 0;
			try {
				toMod = Integer.parseInt(args[3]);
			} catch(Exception e) {
				sendMessage("Invalid Command", "Please enter a valid integer", false, MessageType.ERROR(), channel);
				return;
			}

			if(comm.equals("set")) {
				if(toMod > character.getMaxMP())
					toMod = character.getMaxMP();
				else if(toMod < 0)
					toMod = 0;
				character.setMP(toMod);
				sendMessage("Success", "Setted " + character.getName() + "'s MP to " + toMod, false, eb, channel);
			}

			else if(comm.equals("setmax")) {
				character.setMPmax(toMod);
				sendMessage("Success", "Setted " + character.getName() + "'s Max MP to " + character.getMaxMP() + "", false, eb, channel);
			}

			else if(comm.equals("add")) {
				if(toMod + character.getMP() > character.getMaxMP()) {
					toMod = character.getMaxMP() - character.getMP();
				}
				character.setMP(character.getMP() + toMod);
				sendMessage("Success", "Added " + toMod + " to " + character.getName() + "'s MP\n Now: " + character.getMP() + "", false, eb, channel);
			}
			else if(comm.equals("subtract")) {
				if(character.getMP() - toMod < 0) {
					toMod = character.getMP();
				}
				character.setMP(character.getMP() - toMod);
				sendMessage("Success", "Subtracted " + toMod + " from " + character.getName() + "'s MP\n Now: " + character.getMP() + "", false, eb, channel);
			}
			else {
				sendMessage("Invalid Command", "Invalid comamnd for MP", false, MessageType.ERROR(), channel);
			}
		}

	}

	/**
	 * MP commands for the player
	 * @param channel
	 * @param msg
	 * @param args
	 * @param user
	 */
	private void mpCommands(MessageChannel channel, String msg, String[] args, User user) {
		/**
		 * Commands:
		 *  >mp 
		 *  	-Displays current mp out of max mp
		 */
		ArrayList<String> quoteExtract = extractQuotes(msg);
		EmbedBuilder eb = MessageType.SPELLBOOK();

		PlayerProfile profile = players.get(user.getName());

		//sets default channel to whatever the player's channel is
		if(profile != null && profile.getChannel() != null)
			channel = profile.getChannel();

		//checks player has a profile and a character
		if(profile == null || profile.getHero() == null) {
			sendMessage("No Profile or Character Found", "You must have a profile and a character before you can access your MP", false, MessageType.ERROR(), channel);
			return;
		}

		CharacterData character = profile.getHero();
		character.refresh();
		//if only one argument, meaning just ">mp", displays current MP / max MP
		if(args.length == 1 && quoteExtract.size() == 0) {
			sendMessage(character.getName() + "'s MP", character.getMP() + "/" + character.getMaxMP(), true, eb, channel);
		}
		else
			sendMessage("Invalid Command", "Invalid Command for MP", false, MessageType.ERROR(), channel);

	}

	/**
	 * Cast commands for the player
	 * @param channel
	 * @param msg
	 * @param args
	 * @param user
	 */
	private void castCommands(MessageChannel channel, String msg, String[] args, User user) {
		/**
		 * Commands:
		 * 	>cast "<spell>"
		 * 		-Casts the specified spell and drains MP
		 */
		ArrayList<String> quoteExtract = extractQuotes(msg);
		EmbedBuilder eb = MessageType.SPELLBOOK();

		PlayerProfile profile = players.get(user.getName());

		//sets default channel to whatever the player's channel is
		if(profile != null && profile.getChannel() != null)
			channel = profile.getChannel();

		//checks player has a profile and a character
		if(profile == null || profile.getHero() == null) {
			sendMessage("No Profile or Character Found", "You must have a profile and a character before you can access your MP", false, MessageType.ERROR(), channel);
			return;
		}

		CharacterData character = profile.getHero();
		character.getSpellbook().refresh();
		if(args.length == 2 && quoteExtract.size() == 1) {
			String arg = quoteExtract.get(0);

			if(spells.containsKey(arg) && character.getSpellbook().getSpellbook().containsKey(arg)) {
				boolean success = character.cast(arg);
				if(success)
					sendMessage(arg + " Casted", character.getName() + " casted " + arg + "\n MP: " + character.getMP() + "/" + character.getMaxMP(), false, eb, channel);
				else
					sendMessage("Failed Cast", character.getName() + " does not have enough MP to cast " + arg + " (Cost: " + spells.get(arg).getCost() + ")\n" + "MP: " + character.getMP() +"/" + character.getMaxMP(), false, eb, channel);
			}
			else {
				sendMessage("Cannot find " + arg, "The spell, '" + arg + "' is not in your spellbook\n", true, eb, channel);
			}
		} 
		else {
			sendMessage("Invalid Command", "Invalid Command for Cast", false, MessageType.ERROR(), channel);
		}
	}

	/**
	 * Uses regex to extract all enclosed quote characters
	 * @param msg
	 * @return
	 */
	private ArrayList<String> extractQuotes(String msg) {
		String pattern = "\"([^\"]*)\"";
		ArrayList<String> results = new ArrayList<String>();

		//creates pattern
		Pattern r = Pattern.compile(pattern);

		Matcher m = r.matcher(msg);

		//Extract strings out of quotes
		while(m.find()) {
			String toAdd = msg.substring(m.start(), m.end()-1);
			if(!toAdd.equals("\"") && !toAdd.equals(""))
				toAdd = toAdd.substring(1);
			results.add(toAdd);
		}
		return results;
	}

	/**
	 * Helper to send messages. Sends multiple message is message length is above 1024 characters
	 * @param header
	 * @param msg
	 * @param eb
	 * @param channel
	 */
	private void sendMessage(String header, String msg, boolean sc, EmbedBuilder eb, MessageChannel channel) {
		if(msg.length() + header.length() >= 1015) {
			ArrayList<String> toPrint = new ArrayList<String>();
			int toCnt = 1015 - header.length();
			int index = 0;
			while(msg.length() > index) {
				toPrint.add(msg.substring(index, Math.min(index + toCnt, msg.length())));
				index += toCnt;
			}

			for(int i=0; i<toPrint.size(); i++) {
				if(i == 0) {
					eb.addField(header, toPrint.get(i), sc);
					channel.sendMessage(eb.build()).queue();
					eb.clearFields();
				}
				else {
					eb.addField(header + " - " + (i+1), toPrint.get(i), sc);
					channel.sendMessage(eb.build()).queue();
					eb.clearFields();
				}
			}
		}
		else {
			eb.addField(header, msg, sc);
			channel.sendMessage(eb.build()).queue();
		}
	}

	@Override
	public void onMessageReceived(MessageReceivedEvent msg) {
		Message messageObj = msg.getMessage();
		MessageChannel channelObj = msg.getChannel();
		Member sender = msg.getMember();

		System.out.println("[SYS]: Message Received: '" + msg.getMessage().toString() + "'");
		//first time startup
		if(server == null) {
			server = msg.getGuild();
			updateLists(server);
			EmbedBuilder eb = MessageType.SYSTEM();
			eb.setDescription("Setup Complete");
			System.out.println("[SETUP]: Setup Complete");
			channelObj.sendMessage(eb.build()).queue();
		}

		String message = messageObj.getContentRaw();
		if(message.equals(""))
			return;

		/**
		 * All commands go in this 'if' statement. All commands begins with 
		 * the prefix
		 */
		if(message.substring(0, 1).equals(BotInfo.prefix)) {
			//formats message
			message = message.substring(1);
			message = message.trim();
			message = message.replaceAll("[\\u201C\\u201D]", "\"");

			if(message.equals(""))
				return;

			List<String> list = new ArrayList<String>();
			Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(message);
			while (m.find())
				list.add(m.group(1));
			String[] args = new String[list.size()];
			args = list.toArray(args);

			//check if sender has administrative rights
			boolean isAdmin = false;
			List<Permission> permissions = sender.getPermissions();
			for(Permission per : permissions) {
				if(per.equals(Permission.ADMINISTRATOR)) {
					isAdmin = true;
					break;
				}
			}

			//prepares permission denied message
			EmbedBuilder eb = MessageType.ERROR();
			eb.addField("Permission Denied", "You do not have permission to use or access this command", true);
			String username;
			try {
				switch(args[0]) {

				//ADMIN ONLY COMMANDS
				//**************************************************************************************************

				//commands that list out data in the system
				case "list":
					if(!isAdmin) {
						channelObj.sendMessage(eb.build()).queue();
						return;
					}
					listCommands(channelObj, args);
					break;

					//commands that has to do with the database 
				case "db": 
					if(!isAdmin) {
						channelObj.sendMessage(eb.build()).queue();
						return;
					}
					databaseCommands(channelObj, args);
					break;

					//commands that create new entities
				case "create":
					if(!isAdmin) {
						channelObj.sendMessage(eb.build()).queue();
						return;
					}
					createCommands(channelObj, message, args);
					break;

					//edit entity attributes
				case "edit":
					if(!isAdmin) {
						channelObj.sendMessage(eb.build()).queue();
					}
					editCommands(channelObj, message, args);
					break;

					//commands that assign things
				case "assign":
					if(!isAdmin) {
						channelObj.sendMessage(eb.build()).queue();
						return;
					}
					assignCommands(channelObj, message, args);
					break;

					//commands that show things
				case "show":
					if(!isAdmin) {
						channelObj.sendMessage(eb.build()).queue();
						return;
					}
					showCommands(channelObj, message, args);
					break;

					//commands that searches through system to and matches to passed in string
				case "search":
					if(!isAdmin) {
						channelObj.sendMessage(eb.build()).queue();
						return;
					}
					searchCommands(channelObj, message, args);
					break;

					//shutdown the bot
				case "shutdown":
					if(!isAdmin) {
						channelObj.sendMessage(eb.build()).queue();
						return;
					}
					sendMessage("Shutting down...", "Bot shutting down...", false, MessageType.SYSTEM(), channelObj);
					System.exit(-1);
					break;

					//refreshes all data in the system from DB
				case "refresh":
					if(!isAdmin) {
						channelObj.sendMessage(eb.build()).queue();
						return;
					}
					sendMessage("Performing System Refresh...", "Refreshing...", false, MessageType.SYSTEM(), channelObj);
					channels.clear();
					members.clear();
					players.clear();
					characters.clear();
					items.clear();
					spells.clear();
					updateLists(msg.getGuild());
					for(String c : characters.keySet()) {
						characters.get(c).refresh();
						characters.get(c).getInventory().refresh();
						characters.get(c).getSpellbook().refresh();
					}
					sendMessage("System Refresh Complete", "Refresh Complete and Successful", false, MessageType.SYSTEM(), channelObj);
					break;

					//**************************************************************************************************

					//commands that deal with inventory of characters
				case "inventory":
					if(!isAdmin) {
						inventoryCommands(channelObj, message, args, sender.getUser());
					} else {
						inventoryAdminCommands(channelObj, message, args);
					}
					break;

					//command that shows own profile
				case "profile": 
					username = sender.getUser().getName();
					if(players.containsKey(username)) {
						MessageChannel channel = channelObj;

						if(players.get(username).getChannel() != null) 
							channel = players.get(username).getChannel();

						sendMessage(username + "'s Profile", players.get(username).toString(), false, MessageType.SYSTEM(), channel);
					}
					else {
						sendMessage("Error: No Profile Found", "You do not have a profile", false, MessageType.ERROR(), channelObj);
					}
					break;

					//command that shows on hero
				case "hero":
					username = sender.getUser().getName();
					if(players.containsKey(username) && players.get(username).getHero() != null) {
						MessageChannel channel = channelObj;

						if(players.get(username).getChannel() != null) 
							channel = players.get(username).getChannel();
						CharacterData c = players.get(username).getHero();

						sendMessage(c.getName() + "'s Profile", c.toString(), false, MessageType.SYSTEM(), channel);
					}
					else {
						sendMessage("Error: No Profile or Hero Found", "You do not have a profile or a hero assigned to it", false, MessageType.ERROR(), channelObj);
					}
					break;

					//commands that display information about character spellbooks
				case "spellbook":

					if(!isAdmin) {
						spellbookCommands(channelObj, message, args, sender.getUser());
					} else {
						spellbookAdminCommands(channelObj, message, args);
					}
					break;

					//commands that display and modify information about character mp
				case "mp":
					if(!isAdmin) {
						mpCommands(channelObj, message, args, sender.getUser());
					} else {
						mpAdminCommands(channelObj, message, args);
					}
					break;

					//commands for casting spells from own spellbook
				case "cast":
					castCommands(channelObj, message, args, sender.getUser());
					break;
				}
			} catch(Exception e) {
				eb = MessageType.ERROR();
				String emsg = e.toString();
				sendMessage("FATAL ERROR: EXCEPTION BELOW", emsg, false, eb, channelObj);

			}
		}
	}


	public static void main( String[] args ) {

		try {
			JDA jda = new JDABuilder(AccountType.BOT).setToken(BotInfo.token).buildBlocking();
			jda.addEventListener(new App());

		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("FATAL ERROR: " + e.toString());
		}
	}
}
